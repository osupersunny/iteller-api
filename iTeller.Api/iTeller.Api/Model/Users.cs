﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTeller.Api.Model
{
    public class Users    
    { 
        public int ID { get; set; }
        public string Name { get; set; }
        public string Departmet { get; set; }
        public string Currency { get; set; }
    }
}

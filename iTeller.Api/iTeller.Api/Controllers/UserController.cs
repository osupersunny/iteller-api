﻿using iTeller.Api.Data;
using iTeller.Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserRepository _userRepository;

        public UserController(UserRepository userRepository) => _userRepository = userRepository;


        [HttpGet]
        public ActionResult Get()
        {
            var users = _userRepository.GetUsers();
            return Ok(users);
        }


        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var user = _userRepository.GetUserById(id);

            return Ok(user);
        
        }

        [HttpPost]
        public ActionResult Create(Users user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid State of Entries");

            var users = _userRepository.Create(user);
            return Ok(users);
        }

        [HttpPut]
        public ActionResult Put([FromBody]Users user, int id)
        {
            if(!ModelState.IsValid)
                return BadRequest("Invalid State of Entries");
            if (user.ID != id)
                return BadRequest("Invalid Record");

            var users = _userRepository.Update(user);

            return Ok(users);

        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var user = _userRepository.Delete(id);
            return Ok(user);

        }

        //[HttpGet]
        //public ActionResult Count()
        //{
        //    int count = _userRepository.Count();
        //    return Ok(count);
        //}

    }
}

﻿using iTeller.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTeller.Api.Data
{
    public class UserRepository
    {
        private List<Users> users = new List<Users>
        {
                new Users{ID=1, Currency="USD", Departmet="SD", Name="Sunday Oladiran"},
                new Users{ID=2, Currency="POUNDS", Departmet="SD", Name="Samuel Iyiomere"},
                new Users{ID=3, Currency="NGN", Departmet="SD", Name="Micheal Peters"},
                new Users{ID=4, Currency="USD", Departmet="IT", Name="Hameeda Olabanjo"},

        };

        public List<Users> GetUsers() => users;

        public Users GetUserById(int Id) => users.Find(u => u.ID == Id);

        public List<Users> Create(Users user)
        {
           users.Add(user);
            return users;
        }

        public List<Users> Update(Users user)
        {
            var dbUser = GetUserById(user.ID);
            users.Remove(dbUser);
            users.Add(user);

            return users;
        }

        public List<Users> Delete(int id)
        {
            var user2Remove = GetUserById(id);
            users.Remove(user2Remove);

            return users;
        }

        public int Count() => users.ToArray().Length;
        
    }
}
